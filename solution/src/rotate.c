#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>

struct image init_image() {
    struct image my_image;

    my_image.width = 0;
    my_image.height = 0;
    my_image.data = NULL;
    return my_image;
}

uint64_t get_pixel_cord(struct image const *source, uint64_t x, uint64_t y) {
    return (source->height - y - 1) * source->width + x;
}

struct image rotate(struct image const *source) {
    struct image image_rot = init_image();

    struct pixel *data = malloc(sizeof(struct pixel) * source->height * source->width);
    if (data) {
        for (uint64_t i = 0; i < source->width; i++) {
            for (uint64_t j = 0; j < source->height; j++) {
                data[i * source->height + j] = source->data[get_pixel_cord(source, i, j)];
            }
        }
        image_rot.width = source->height;
        image_rot.height = source->width;
        image_rot.data = data;
    } else {
        fprintf(stderr, "Error out of memory! (rotate: malloc: data ");
    }
    return image_rot;
}
