#include "../include/read.h"
#include "../include/rotate.h"
#include "../include/write.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    struct image source, result;

    if (argc != 3) fprintf(stderr, "Invalid input");
    else {
        enum read_status read_stat = from_bmp_adapter(argv[1], &source);
        if (!read_stat) {
            result = rotate(&source);
            if (result.data != NULL) {
                enum write_status write_stat = to_bmp_adapter(argv[2], &result);
                if (write_stat) fprintf(stderr, "Write to .bmp Error (code %d)!", write_stat);
                free(result.data);
            } else fprintf(stderr, "Rotate image Error!");
        } else fprintf(stderr, "Read from .bmp Error (code %d)!", read_stat);
        free(source.data);
    }

    return 0;
}
