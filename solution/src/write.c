#include "../include/bmp_file.h"
#include "../include/write.h"
#include <string.h>

enum write_status to_bmp_adapter(const char *filename, struct image *img) {
    enum write_status result;
    FILE *out;

    out = fopen(filename, "wb");
    if (out) {
        result = to_bmp(out, img);
        fclose(out);
    } else {
        fprintf(stderr, "Open file Error! (to_bmp_adapter: fopen: file) ");
        result = WRITE_ERROR;
    }
    return result;
}

struct bmp_file get_bmp_head(struct image const *img, const uint8_t padding) {
    struct bmp_file head;

    uint32_t biSizeImage = (sizeof(struct pixel) * img->width + padding) * img->height;

    head.bfType = 0x4D42;
    head.biSize = 40;
    head.biPlanes = 1;
    head.biClrUsed = 0;
    head.bfReserved = 0;
    head.biBitCount = 24;
    head.biCompression = 0;
    head.biClrImportant = 0;
    head.biXPelsPerMeter = 0;
    head.biYPelsPerMeter = 0;
    head.biWidth = img->width;
    head.biHeight = img->height;
    head.biSizeImage = biSizeImage;
    head.bOffBits = sizeof(struct bmp_file);
    head.bfileSize = sizeof(struct bmp_file) + biSizeImage;
    return head;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_file head;
    enum write_status result = WRITE_OK;
    char error[60];
    size_t written;

    const uint8_t padding = get_padding(img->width);
    head = get_bmp_head(img, padding);
    written = fwrite(&head, sizeof(struct bmp_file), 1, out);
    if (written == 1) {
        for (uint64_t i = 0; i < img->height; i++) {
            written = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
            uint64_t write_cnt = img->width;

            if (padding) {
                uint8_t padding_bytes[3] = {0};
                written += fwrite(&padding_bytes, sizeof(uint8_t), padding, out);
                write_cnt += padding;
            }

            if (written != write_cnt) {
                strcpy(error, "Write file Error! (to_bmp: fwrite(2): written != write_cnt)");
                result = WRITE_ERROR;
                i = img->height;
            }
        }
        if (result != WRITE_OK) fprintf(stderr, "%s", error);
    } else {
        fprintf(stderr, "Write file Error! (to_bmp: fwrite(1): written == 1) ");
        result = WRITE_ERROR;
    }
    return result;
}
