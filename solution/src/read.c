#include "../include/bmp_file.h"
#include "../include/read.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>

enum read_status from_bmp_adapter(const char *filename, struct image *img) {
    enum read_status result;
    FILE *in;

    in = fopen(filename, "rb");
    if (in) {
        result = from_bmp(in, img);
        fclose(in);
    } else {
        fprintf(stderr, "Open file Error! (from_bmp_adapter: fopen: file)");
        result = READ_ERROR;
    }
    return result;
}

enum read_status read_head(FILE *in, struct bmp_file *head) {
    enum read_status result = READ_OK;

    size_t read = fread(head, sizeof(struct bmp_file), 1, in);
    if (read != 1) {
        fprintf(stderr, "Read file Error! (read_head: fread: read != 1)");
        result = READ_INVALID_HEADER;
    }
    switch (errno) {
        case EACCES: {
            fprintf(stderr, "Read file Error! (read_head: case EACCES");
            result = READ_INVALID_HEADER;
            break;
        }
        case EFBIG: {
            fprintf(stderr, "Read file Error! (read_head: case EFBIG)");
            result = READ_INVALID_HEADER;
            break;
        }
        case EISDIR: {
            fprintf(stderr, "Read file Error! (read_head: case EISDIR)");
            result = READ_INVALID_HEADER;
            break;
        }
        case ENAMETOOLONG: {
            fprintf(stderr, "Read file Error! (read_head: case ENAMETOOLONG)");
            result = READ_INVALID_HEADER;
            break;
        }
        case ENFILE: {
            fprintf(stderr, "Read file Error! (read_head: case ENFILE)");
            result = READ_INVALID_HEADER;
            break;
        }
        case ENOENT: {
            fprintf(stderr, "Read file Error! (read_head: case ENOENT)");
            result = READ_INVALID_HEADER;
            break;
        }
    }
    return result;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_file head;
    enum read_status result;
    char error[60];
    size_t read;

    result = read_head(in, &head);
    if (result == READ_OK) {
        const uint8_t padding = get_padding(head.biWidth);
        struct pixel *data = malloc(sizeof(struct pixel) * head.biWidth * head.biHeight);
        if (data) {
            for (uint64_t i = 0; i < head.biHeight; i++) {
                read = fread(&data[i * head.biWidth], sizeof(struct pixel), head.biWidth, in);
                if (read != head.biWidth) {
                    strcpy(error, "Read file Error! (from_bmp: fread: read != head.biWidth) ");
                    result = READ_INVALID_SIGNATURE;
                    i = img->height;
                }
                if (padding) {
                    if (fseek(in, (long) padding, SEEK_CUR)) {
                        strcpy(error, "Offset in file Error! (from_bmp: fseek: fseek != 0) ");
                        result = READ_ERROR;
                        i = img->height;
                    }
                }
            }

            if (result != READ_OK) fprintf(stderr, "%s", error);

            img->width = head.biWidth;
            img->height = head.biHeight;
            img->data = data;
        } else {
            fprintf(stderr, "Error out of memory! (from_bmp: malloc: data)");
            result = MEMORY_ERROR;
        }
    }
    return result;
}
