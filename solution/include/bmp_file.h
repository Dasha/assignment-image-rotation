#ifndef LAB_1_BMP_FILE_H
#define LAB_1_BMP_FILE_H

#include <stdint.h>

static uint8_t get_padding(uint64_t width) {
    uint8_t padding = 3 * width % 4;
    if (padding) padding = 4 - padding;
    return padding;
}

#pragma pack(push, 1)
struct bmp_file {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

#endif //LAB_1_BMP_FILE_H
