#ifndef LAB_1_READ_H
#define LAB_1_READ_H

#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    MEMORY_ERROR,
    READ_ERROR
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE *in, struct image *img);

enum read_status from_bmp_adapter(const char *filename, struct image *img);

#endif //LAB_1_READ_H
