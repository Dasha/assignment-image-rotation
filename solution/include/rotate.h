#ifndef LAB_1_ROTATE_H
#define LAB_1_ROTATE_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const *source);

#endif //LAB_1_ROTATE_H
