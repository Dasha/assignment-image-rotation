#ifndef LAB_1_WRITE_H
#define LAB_1_WRITE_H

#include "image.h"
#include <stdio.h>

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE *out, struct image const *img);

enum write_status to_bmp_adapter(const char *filename, struct image *img);

#endif //LAB_1_WRITE_H
